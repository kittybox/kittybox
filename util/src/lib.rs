use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize)]
pub struct IndiewebEndpoints {
    pub authorization_endpoint: String,
    pub token_endpoint: String,
    pub webmention: Option<String>,
    pub microsub: Option<String>,
}

/// Data structure representing a Micropub channel in the ?q=channels output.
#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct MicropubChannel {
    /// The channel's UID. It is usually also a publically accessible permalink URL.
    pub uid: String,
    /// The channel's user-friendly name used to recognize it in lists.
    pub name: String,
}
