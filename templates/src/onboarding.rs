markup::define! {
    OnboardingPage {
        h1[style="text-align: center"] {
            "Welcome to Kittybox"
        }
        script[type="module", src="/static/onboarding.js"] {}
        link[rel="stylesheet", href="/static/onboarding.css"];
        form.onboarding[action="", method="POST"] {
            noscript {
                p {
                    "Ok, let's be honest. Most of this software doesn't require JS to be enabled "
                    "to view pages (and in some cases, even edit them if logged in)."
                }
                p { "This page is a little bit different. It uses JavaScript to provide interactive features, such as:" }
                ul {
                    li { "Multiple-input questions" }
                    li { "Answers spanning multiple fields" }
                    li { "Preview of files being uploaded" }
                    li { "Pretty pagination so you won't feel overwhelmed" }
                }
                p {
                    "Sadly, it's very hard or even impossible to recreate this without any JavaScript. "
                    "Good news though - the code is " b { "open-source AND free software" }
                    " (under GNU AGPLv3) "
                    "and I promise to not obfuscate it or minify it. "
                    a[href="/static/onboarding.js"] { "Here" }
                    "'s the link - you can try reading it so you'll be 200% sure "
                    "it won't steal your cookies or turn your kitty into a soulless monster."
                    @markup::raw("<!-- do cats even have souls? I'm not sure. But this code won't steal their souls anyway. -->")
                }
                hr;
                p { "In other words: " b { "please enable JavaScript for this page to work properly." } small { "sorry T__T" } }
            }
            ul #progressbar[style="display: none"] {
                li #intro { "Introduction" }
                li #hcard { "Your profile" }
                li #settings { "Your website" }
                li #firstpost { "Your first post" }
            }
            fieldset #intro[style="display: none"] {
                legend { "Introduction" }
                p {
                    "Kittybox is a CMS that can act as a member of the IndieWeb. "
                    "IndieWeb is a global distributed social network built on top of open Web standards "
                    "and composed of blogs around the Internet supporting these standards."
                }
                p { "There is no registration or centralized database of any sort - everyone owns their data and is responsible for it." }
                p { "If you're seeing this page, it looks like your configuration is correct and we can proceed with the setup." }

                div.switch_card_buttons {
                    button.switch_card.next_card[type="button", "data-card"="hcard"] { "Next" }
                }
            }

            fieldset #hcard[style="display: none"] {
                legend { "Your profile" }
                p { "An h-card is an IndieWeb social profile, and we're gonna make you one!" }
                p { "Thanks to some clever markup, it will be readable by both humans and machines looking at your homepage."}
                p {
                    "If you make a mistake, don't worry, you're gonna be able to edit this later."
                    "The only mandatory field is your name."
                }

                div.form_group {
                    label[for="hcard_name"] { "Your name" }
                    input #hcard_name[name="hcard_name", placeholder="Your name"];
                    small {
                        "No need to write the name as in your passport, this is not a legal document "
                        "- just write how you want to be called on the network. This name will be also "
                        "shown whenever you leave a comment on someone else's post using your website."
                    }
                }

                div.form_group {
                    label[for="pronouns"] { "Your pronouns" }
                    div.multi_input #pronouns {
                        template {
                            input #hcard_pronouns[name="hcard_pronouns", placeholder="they/them?"];
                        }
                        button.add_more[type="button", "aria-label"="Add more"] { "[+] Add more" }
                    }
                    small {
                        "Write which pronouns you use for yourself. It's a free-form field "
                        "so don't feel constrained - but keep it compact, as it'll be shown in a lot of places."
                    }
                }

                div.form_group {
                    label[for="urls"] { "Links to other pages of you" }
                    div.multi_input #urls {
                        template {
                            input #hcard_url[name="hcard_url", placeholder="https://example.com/"];
                        }
                        button.add_more[type="button", "aria-label"="Add more"] { "[+] Add more" }
                    }
                    small {
                        "These URLs will help your readers find you elsewhere and will help you that whoever owns these pages owns your website too"
                        " in case the links are mutual. So make sure to put a link to your site in your other social profiles!"
                    }
                }

                div.form_group {
                    label[for="hcard_note"] { "A little about yourself" }
                    textarea #hcard_note[name="hcard_note", placeholder="Loves cooking, plants, cats, dogs and racoons."] {}
                    small { "A little bit of introduction. Just one paragraph, and note, you can't use HTML here (yet)." }
                    // TODO: HTML e-note instead of p-note
                }

                // TODO: u-photo upload - needs media endpoint cooperation

                div.switch_card_buttons {
                    button.switch_card.prev_card[type="button", "data-card"="intro"] { "Previous" }
                    button.switch_card.next_card[type="button", "data-card"="settings"] { "Next" }
                }
            }

            fieldset #settings[style="display: none"] {
                legend { "Your website" }
                p { "Ok, it's nice to know you more. Tell me about what you'll be writing and how you want to name your blog." }
                // TODO: site-name, saved to settings

                div.form_group {
                    label[for="blog_name"] { "Your website's name"}
                    input #blog_name[name="blog_name", placeholder="Kitty Box!"];
                    small { "It'll get shown in the title of your blog, in the upper left corner!" }
                }

                div.form_group {
                    label[for="custom_feeds"] { "Custom feeds" }
                    small {
                        p {
                            "You can set up custom feeds to post your stuff to. "
                            "This is a nice way to organize stuff into huge folders, like all your trips or your quantified-self data."
                        }
                        p {
                            "Feeds can be followed individually, which makes it easy for users who are interested in certain types "
                            "of content you produce to follow your adventures in certain areas of your life without cluttering their "
                            "readers."
                        }
                        p {
                            "We will automatically create some feeds for you aside from these so you won't have to - including a main feed, "
                            "address book (for venues you go to and people you talk about), a cookbook for your recipes and some more."
                            // TODO: Put a link to documentation explaining feeds in more detail.
                        }
                    }
                    div.multi_input #custom_feeds {
                        template {
                            fieldset.feed {
                                div.form_group {
                                    label[for="feed_name"] { "Name" }
                                    input #feed_name[name="feed_name", placeholder="My cool feed"];
                                    small { "This is a name that will identify this feed to the user. Make it short and descriptive!" }
                                }
                                div.form_group {
                                    label[for="feed_slug"] { "Slug" }
                                    input #feed_slug[name="feed_slug", placeholder="my-cool-feed"];
                                    small { "This will form a pretty URL for the feed. For example: https://example.com/feeds/my-cool-feed" }
                                }
                            }
                        }
                        button.add_more[type="button", "aria-label"="Add more"] { "[+] Add More" }
                    }
                }

                div.switch_card_buttons {
                    button.switch_card.prev_card[type="button", "data-card"="hcard"] { "Previous" }
                    button.switch_card.next_card[type="button", "data-card"="firstpost"] { "Next" }
                }
            }

            fieldset #firstpost[style="display: none"] {
                legend { "Your first post" }
                p { "Maybe you should start writing your first posts now. How about a short note?" }
                p { "A note is a short-form post (not unlike a tweet - but without the actual character limit) that doesn't bear a title." }
                p {
                    "Consider telling more about yourself, your skills and interests in this note "
                    @markup::raw("&mdash;")
                    " though you're free to write anything you want. (By the way, you can use "
                    a[href="https://daringfireball.net/projects/markdown/syntax"] { "Markdown" }
                    " here to spice up your note!)"
                }

                textarea #first_post_content[style="width: 100%; height: 8em", placeholder="Hello! I am really excited about #IndieWeb"] {}

                div.switch_card_buttons {
                    button.switch_card.prev_card[type="button", "data-card"="settings"] { "Previous" }
                    button[type="submit"] { "Finish" }
                }
            }
        }
    }
}
