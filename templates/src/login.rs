markup::define! {
    LoginPage {
        form[method="POST"] {
            h1 { "Sign in with your website" }
            p {
                "Signing in to Kittybox might allow you to view private content "
                    "intended for your eyes only."
            }

            section {
                label[for="url"] { "Your website URL" }
                input[id="url", name="url", placeholder="https://example.com/"];
                input[type="submit"];
            }
        }
    }
}
