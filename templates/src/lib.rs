mod templates;
pub use templates::{ErrorPage, MainPage, Template, POSTS_PER_PAGE, Entry, VCard, Feed};
mod onboarding;
pub use onboarding::OnboardingPage;
mod login;
pub use login::LoginPage;
