#!/bin/sh
export RUST_LOG="info,kittybox=debug,retainer::cache=warn,h2=warn,rustls=warn"
export BACKEND_URI=file://./test-dir
export TOKEN_ENDPOINT=https://tokens.indieauth.com/token
export AUTHORIZATION_ENDPOINT=https://indieauth.com/auth
export COOKIE_SECRET=1234567890abcdefghijklmnopqrstuvwxyz
#export COOKIE_SECRET_FILE=/dev/null
if [[ "$1" == "watch" ]]; then
	shift
	exec systemfd --no-pid -s http::8080 -- cargo watch -x run "$@"
elif [[ "$1" == "run" ]]; then
	shift 1
	exec cargo run "$@"
else
	echo "Usage: ./dev.sh [watch|run]"
	exit 1
fi
