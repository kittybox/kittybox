#!/usr/bin/env bash
set -e
cd fonts
url='https://fonts.google.com/download?family=Lato|Caveat|Noto%20Color%20Emoji|Noto%20Colr%20Emoji%20Glyf'
tmp=$(mktemp)
curl "$url" > "$tmp"
unzip -uo "$tmp"
rm "$tmp"
