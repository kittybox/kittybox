#![allow(unused_imports, dead_code)]
use async_trait::async_trait;
use lazy_static::lazy_static;
use std::time::{Duration, Instant};
use prometheus::Encoder;

// TODO: Vendor in the Metrics struct from warp_prometheus and rework the path matching algorithm

pub fn metrics(path_includes: Vec<String>) -> warp::log::Log<impl Fn(warp::log::Info) + Clone> {
    let metrics = warp_prometheus::Metrics::new(prometheus::default_registry(), &path_includes);
    warp::log::custom(move |info| metrics.http_metrics(info))
}

pub fn gather() -> Vec<u8> {
    let mut buffer: Vec<u8> = vec![];
    let encoder = prometheus::TextEncoder::new();
    let metric_families = prometheus::gather();
    encoder.encode(&metric_families, &mut buffer).unwrap();

    buffer
}
