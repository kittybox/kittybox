const firstOnboardingCard = "intro";

function switchOnboardingCard(card) {
    Array.from(document.querySelectorAll("form.onboarding > fieldset")).map(node => {
        if (node.id == card) {
            node.style.display = "block";
        } else {
            node.style.display = "none";
        }
    });

    Array.from(document.querySelectorAll("form.onboarding > ul#progressbar > li")).map(node => {
        if (node.id == card) {
            node.classList.add("active")
        } else {
            node.classList.remove("active")
        }
    })
};

window.kittybox_onboarding = {
    switchOnboardingCard
};

document.querySelector("form.onboarding > ul#progressbar").style.display = "";
switchOnboardingCard(firstOnboardingCard);

function switchCardOnClick(event) {
    switchOnboardingCard(event.target.dataset.card)
}

function multiInputAddMore(event) {
    let parent = event.target.parentElement;
    let template = event.target.parentElement.querySelector("template").content.cloneNode(true);
    parent.prepend(template);
}

Array.from(document.querySelectorAll("form.onboarding > fieldset button.switch_card")).map(button => {
    button.addEventListener("click", switchCardOnClick)
})

Array.from(document.querySelectorAll("form.onboarding > fieldset div.multi_input > button.add_more")).map(button => {
    button.addEventListener("click", multiInputAddMore)
    multiInputAddMore({ target: button });
})

const form = document.querySelector("form.onboarding");
console.log(form);
form.onsubmit = async (event) => {
    console.log(event);
    event.preventDefault();
    const form = event.target;
    const json = {
        user: {
            type: ["h-card"],
            properties: {
                name: [form.querySelector("#hcard_name").value],
                pronoun: Array.from(form.querySelectorAll("#hcard_pronouns")).map(input => input.value).filter(i => i != ""),
                url: Array.from(form.querySelectorAll("#hcard_url")).map(input => input.value).filter(i => i != ""),
                note: [form.querySelector("#hcard_note").value]
            }
        },
        first_post: {
            type: ["h-entry"],
            properties: {
                content: [form.querySelector("#first_post_content").value]
            }
        },
        blog_name: form.querySelector("#blog_name").value,
        feeds: Array.from(form.querySelectorAll(".multi_input#custom_feeds > fieldset.feed")).map(form => {
            return {
                name: form.querySelector("#feed_name").value,
                slug: form.querySelector("#feed_slug").value
            }
        }).filter(feed => feed.name == "" || feed.slug == "")
    };

    await fetch("/", {
        method: "POST",
        body: JSON.stringify(json),
        headers: { "Content-Type": "application/json" }
    }).then(response => {
        if (response.status == 201) {
            window.location.href = window.location.href;
        }
    })
}