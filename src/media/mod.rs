use futures_util::StreamExt;
use bytes::buf::Buf;
use warp::{Filter, Rejection, Reply, multipart::{FormData, Part}};

pub fn query() -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::get()
        .and(crate::util::require_host())
        .map(|host| "media endpoint query...")
}

pub fn options() -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::options()
        .map(|| warp::reply::json::<Option<()>>(&None))
        .with(warp::reply::with::header("Allow", "GET, POST"))
}

pub fn upload() -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::post()
        .and(crate::util::require_host())
        .and(warp::multipart::form().max_length(1024*1024*150/*mb*/))
        .and_then(|host, mut form: FormData| async move {
            // TODO get rid of the double unwrap() here
            let file: Part = form.next().await.unwrap().unwrap();
            log::debug!("Uploaded: {:?}, type: {:?}", file.filename(), file.content_type());

            let mut data = file.stream();
            while let Some(buf) = data.next().await {
                // TODO save it into a file
                log::debug!("buffer length: {:?}", buf.map(|b| b.remaining()));
            }
            Ok::<_, warp::Rejection>(warp::reply::with_header(
                warp::reply::with_status(
                    "",
                    warp::http::StatusCode::CREATED
                ),
                "Location",
                "./awoo.png"
            ))
        })
}

pub fn media() -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    upload()
        .or(query())
        .or(options())
}
